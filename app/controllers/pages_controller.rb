class PagesController < ApplicationController
  NAME_FUNCTION_CONTROLLER = "index"

  def index
    @controller = NAME_FUNCTION_CONTROLLER
  end

  def form
    @controller = "form"
  end
end
