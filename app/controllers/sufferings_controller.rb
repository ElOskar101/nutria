class SufferingsController < ApplicationController
  before_action :set_suffering, only: [:show, :edit, :update, :destroy]

  # GET /sufferings
  # GET /sufferings.json
  def index
    @sufferings = Suffering.all
  end

  # GET /sufferings/1
  # GET /sufferings/1.json
  def show
  end

  # GET /sufferings/new
  def new
    @suffering = Suffering.new
  end

  # GET /sufferings/1/edit
  def edit
  end

  # POST /sufferings
  # POST /sufferings.json
  def create
    @suffering = Suffering.new(suffering_params)

    respond_to do |format|
      if @suffering.save
        format.html { redirect_to @suffering, notice: 'Suffering was successfully created.' }
        format.json { render :show, status: :created, location: @suffering }
      else
        format.html { render :new }
        format.json { render json: @suffering.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sufferings/1
  # PATCH/PUT /sufferings/1.json
  def update
    respond_to do |format|
      if @suffering.update(suffering_params)
        format.html { redirect_to @suffering, notice: 'Suffering was successfully updated.' }
        format.json { render :show, status: :ok, location: @suffering }
      else
        format.html { render :edit }
        format.json { render json: @suffering.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sufferings/1
  # DELETE /sufferings/1.json
  def destroy
    @suffering.destroy
    respond_to do |format|
      format.html { redirect_to sufferings_url, notice: 'Suffering was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_suffering
      @suffering = Suffering.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def suffering_params
      params.require(:suffering).permit(:name, :url_img, :code)
    end
end
