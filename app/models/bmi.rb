class Bmi
  # Definition of constants
  CODE_WRONG_EGE = 50
  CODE_WRONG_RANK = 60
  CODE_UNDER_WEIGHT = 100
  CODE_NORMOPESO = 200
  CODE_OVERWEIGHT = 300
  CODE_OBESITY = 400

  WRONG_EGE_MESSAGE = "La edad definida no se encuetra entre el rango validos"
  WRONG_RANK_BMI = "El valor de su indice de masa corporal es incorrecto"
  SEX_MAN = 1

  def initialize (weight, age, height, sex)
    @weight = weight
    @age = age
    @height = height
    @sex = sex
    @bmi = calculate_bmi
    @result = ""
    @code = 0
    # Variable of ERROR message
    @message = ""

    if age >= 19
      define_adult_rank
    else
      define_range_bmi
    end
    puts("paso 1")
  end

  def calculate_bmi
    bmi = @weight.to_f  / (@height * @height)
    puts("El valor de imc es: "+bmi.to_s)
    bmi
  end

  def define_range_bmi
    rank_in_men = []
    rank_in_woman = []

    case @age
      when 3
        rank_in_men = [13.0,19.0,21.3]
        rank_in_woman = [14.6,18.2,19.8]
      when 4
        rank_in_men = [13.4,17.9,19.0]
        rank_in_woman = [13.9,18.7,22.8]
      when 5
        rank_in_men = [14.5,17.9,22.8]
        rank_in_woman = [13.3,18.5,19.0]
      when 6
        rank_in_men = [14.4,19.7,21.3]
        rank_in_woman = [13.6,19.4,21.9]
      when 7
        rank_in_men = [13.7,19.2,24.4]
        rank_in_woman = [14.8,19.5,22.8]
      when 8
        rank_in_men = [14.1,22.0,24.6]
        rank_in_woman = [14.3,20.6,21.7]
      when 9
        rank_in_men = [15.1,21.9,22.8]
        rank_in_woman = [14.1,21.3,21.4]
      when 10
        rank_in_men = [14.8,20.4,23.7]
        rank_in_woman = [14.5,21.9,25.0]
      when 11
        rank_in_men = [15.4,22.7,23.7]
        rank_in_woman = [15.1,21.8,24.6]
      when 12
        rank_in_men = [16.3,26.2,28.0]
        rank_in_woman = [15.5,22.1,24.7]
      when 13
        rank_in_men = [16.4,24.8,27.2]
        rank_in_woman = [16.6,23.5,26.9]
      when 14
        rank_in_men = [16.2,28.1,30.4]
        rank_in_woman = [17.2,23.8,27.4]
      when 15
        rank_in_men = [16.7,26.0,30.4]
        rank_in_woman = [18.3,22.3,25.3]
      when 16
        rank_in_men = [17.7,26.8,31.1]
        rank_in_woman = [17.7,23.9,27.8]
      when 17
        rank_in_men = [18.1,25.6,28.3]
        rank_in_woman = [18.2,24.6,28.9]
      when 18
        rank_in_men = [18.1,25.1,30.1]
        rank_in_woman = [18.0,25.7,28.3]
      else
        @code = CODE_WRONG_EGE
        @message = WRONG_EGE_MESSAGE
    end
    define_sex(rank_in_men, rank_in_woman)
  end

  def define_adult_rank
    rank_in_men = []
    rank_in_woman = []

    case @age
      when 19 .. 24
        rank_in_men = [14.9,19.0,23.3]
        rank_in_woman = [18.9,25.0,29.6]
      when 25 .. 29
        rank_in_men = [16.5,20.3,24.4]
        rank_in_woman = [18.9,25.4,29.8]
      when 30 .. 34
        rank_in_men = [18.0,21.5,25.2]
        rank_in_woman = [19.7,26.4,30.5]
      when 35 .. 39
        rank_in_men = [19.4,22.6,26.1]
        rank_in_woman = [21.0,27.7,31.5]
      when 40 .. 44
        rank_in_men = [20.5,23.6,26.9]
        rank_in_woman = [22.6,29.3,32.8]
      when 45 .. 49
        rank_in_men = [21.5,24.5,27.6]
        rank_in_woman = [27.4,34,37.3]
      when 50 .. 59
        rank_in_men = [22.7,25.6,28.7]
        rank_in_woman = [27.4,34,37.3]
      else
        @code = CODE_WRONG_EGE
        @message = WRONG_EGE_MESSAGE
    end
    define_sex(rank_in_men, rank_in_woman)
  end

  def define_sex(rank_in_men, rank_in_woman)
    if @sex == SEX_MAN
      define_result(rank_in_men)
    else
      define_result(rank_in_woman)
    end
  end

  def define_result (rank)
    case @bmi
      when rank[0] .. rank[1]
        @result = "Normopeso"
        @code = CODE_NORMOPESO
      when rank[1] .. rank[2]
        @result = "Sobrepeso"
        @code = CODE_OVERWEIGHT
      else
        if @bmi < rank[0]
          @result = "Bajo peso"
          @code = CODE_UNDER_WEIGHT
        elsif @bmi  >rank[2]
          @result = "Obesidad"
          @code = CODE_OBESITY
        else
          @code = CODE_WRONG_RANK
          @message = WRONG_RANK_BMI
        end
    end
    @result
    puts("RESULTADO: "+@result)
  end

  def show_result
    result = @result
  end

  def show_code
    code = @code
  end

  CODE_WRONG_EGE = 50
  CODE_WRONG_RANK = 60
  CODE_UNDER_WEIGHT = 100
  CODE_NORMOPESO = 200
  CODE_OVERWEIGHT = 300
  CODE_OBESITY = 400

  def get_code_wrong_age
    CODE_WRONG_EGE
  end

  def get_code_wrong_rank
    CODE_WRONG_RANK
  end

  def get_code_under_weight
    CODE_UNDER_WEIGHT
  end

  def get_code_normopeso
    CODE_NORMOPESO
  end

  def get_code_overweight
    CODE_OVERWEIGHT
  end

  def get_code_obesity
    CODE_OBESITY
  end

  def get_bmi
    @bmi
  end
end