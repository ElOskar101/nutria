class Food < ApplicationRecord
  belongs_to :suffering
  has_many :ingredients
end
