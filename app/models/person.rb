class Person < ApplicationRecord
  belongs_to :country

  # validaciones

  validates :height, presence: true
  validates :weight, presence: true
  validates :age, presence: true
  validates :country_id, presence: true
  validates :abdominal_perim, presence: true
  validates :age, presence: true

end
