class Sport < ApplicationRecord
  belongs_to :suffering

  validates :acceptance, presence: true
  validates :description, presence: true
  validates :name, presence: true
  validates :url_img, presence: true

end
