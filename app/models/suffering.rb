class Suffering < ApplicationRecord
  has_many :recommendations
  has_many :foods
  has_many :sports
end
