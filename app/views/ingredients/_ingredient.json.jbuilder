json.extract! ingredient, :id, :name, :api_code, :created_at, :updated_at
json.url ingredient_url(ingredient, format: :json)
