json.extract! person, :id, :first_name, :last_name, :height, :weight, :abdominal_perim, :imc, :created_at, :updated_at
json.url person_url(person, format: :json)
