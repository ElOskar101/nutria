json.extract! recommendation, :id, :description, :url_img, :created_at, :updated_at
json.url recommendation_url(recommendation, format: :json)
