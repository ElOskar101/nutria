json.extract! sport, :id, :description, :url_img, :name, :acceptance, :created_at, :updated_at
json.url sport_url(sport, format: :json)
