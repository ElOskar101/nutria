json.extract! suffering, :id, :description, :url_img, :created_at, :updated_at
json.url suffering_url(suffering, format: :json)
