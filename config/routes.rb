Rails.application.routes.draw do

  resources :ingredients
  resources :foods
  resources :sports
  resources :sufferings
  resources :recommendations
  resources :countries
  resources :registrations
  resources :people
  devise_for :users, :controller => {sessions: 'sessions' }

  get 'pages/index'
  get 'pages/form'
  get 'pages/feeding'
  get 'pages/sports_page'

  root 'pages#index'
  get '/feeding', to: 'pages#feeding', as: 'feeding'
  get '/sports_page', to: 'pages#sports', as: 'sportss'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
