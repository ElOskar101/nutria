class CreatePeople < ActiveRecord::Migration[5.1]
  def change
    create_table :people do |t|
      t.string :first_name
      t.string :last_name
      t.float :height
      t.float :weight
      t.float :abdominal_perim
      t.float :imc
      t.integer :country_id

      t.timestamps
    end
  end
end
