class CreateRecommendations < ActiveRecord::Migration[5.1]
  def change
    create_table :recommendations do |t|
      t.text :description
      t.string :url_img

      t.timestamps
    end
  end
end
