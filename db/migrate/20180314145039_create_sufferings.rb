class CreateSufferings < ActiveRecord::Migration[5.1]
  def change
    create_table :sufferings do |t|
      t.string :description
      t.string :url_img

      t.timestamps
    end
  end
end
