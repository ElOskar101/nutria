class AddcountryIdToPerson < ActiveRecord::Migration[5.1]
  def change
    add_column :people, :country_id, :integer
  end
end
