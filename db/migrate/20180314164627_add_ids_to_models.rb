class AddIdsToModels < ActiveRecord::Migration[5.1]
  def change
    add_column :sufferings, :person_id, :integer
    add_column :recommendations, :suffering_id, :integer
    add_column :foods, :suffering_id, :integer
    add_column :sports, :suffering_id, :integer
    add_column :ingredients, :food_id, :integer
  end
end
