class AddattributeToPerson < ActiveRecord::Migration[5.1]
  def change
    add_column :people, :sex, :integer
    add_column :people, :age, :integer
  end
end
