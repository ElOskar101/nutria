class RemoveImcFromPeople < ActiveRecord::Migration[5.1]
  def change
    remove_column :people, :imc, :float
  end
end
