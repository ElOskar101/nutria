class RenameBmiFromPeople < ActiveRecord::Migration[5.1]
  def change
    remove_column :people, :bmi, :float
    add_column :people, :bmi, :string
  end
end
