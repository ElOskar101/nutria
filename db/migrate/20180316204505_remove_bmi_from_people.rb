class RemoveBmiFromPeople < ActiveRecord::Migration[5.1]
  def change
    remove_column :people, :bmi, :float
  end
end
