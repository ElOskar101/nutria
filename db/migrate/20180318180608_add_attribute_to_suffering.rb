class AddAttributeToSuffering < ActiveRecord::Migration[5.1]
  def change
    remove_column :sufferings, :person_id, :integer
    add_column :sufferings, :code, :integer
  end
end
