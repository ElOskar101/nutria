class AddAttributeToSufferings < ActiveRecord::Migration[5.1]
  def change
    remove_column :sufferings, :description, :string
    add_column :sufferings, :name, :text
  end
end
