require 'test_helper'

class SufferingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @suffering = sufferings(:one)
  end

  test "should get index" do
    get sufferings_url
    assert_response :success
  end

  test "should get new" do
    get new_suffering_url
    assert_response :success
  end

  test "should create suffering" do
    assert_difference('Suffering.count') do
      post sufferings_url, params: { suffering: { description: @suffering.description, url_img: @suffering.url_img } }
    end

    assert_redirected_to suffering_url(Suffering.last)
  end

  test "should show suffering" do
    get suffering_url(@suffering)
    assert_response :success
  end

  test "should get edit" do
    get edit_suffering_url(@suffering)
    assert_response :success
  end

  test "should update suffering" do
    patch suffering_url(@suffering), params: { suffering: { description: @suffering.description, url_img: @suffering.url_img } }
    assert_redirected_to suffering_url(@suffering)
  end

  test "should destroy suffering" do
    assert_difference('Suffering.count', -1) do
      delete suffering_url(@suffering)
    end

    assert_redirected_to sufferings_url
  end
end
